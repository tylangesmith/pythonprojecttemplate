from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='projectname',
   version='0.1',
   license="MIT",
   long_description=long_description,
   description='',
   author='Ty Lange-Smith',
   author_email='tylangesmith@gmail.com',
   packages=['project'],
   install_requires=[]
)